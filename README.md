- In ideal situation all services should have unit test as minimal as HotelServiceImpl
- All request and response object should be separated dto as SearchHotelReqest
- I wanted to implement JWT security but not have enough time
- I added code for better exception handling, but I have not used it all

- Also I prepare a lot of code when I implement login func
- If I had more time I would have implement Flyway database migration script
- also there should be application-dev.yml, prod and all environment properties
- I add @Validation annotations but also have never actually used it (I wanted to add checks in dto objects)
- ...

- To run this you need running MySql on port 3306 (if you have Docker just run docker-compose file)
- Java 8+
- MVN 3.6+

- Go to HotelApplication and run main function from your IDE
- App should start project on port 8090
- You can see Swagger UI on the URL http://localhost:8090/swagger-ui.html#
