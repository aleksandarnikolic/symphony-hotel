package is.symphony.hotel.service.impl;

import is.symphony.hotel.domain.Address;
import is.symphony.hotel.domain.hotel.Hotel;
import is.symphony.hotel.repository.HotelRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Date;
import java.util.Optional;

import static org.mockito.Mockito.when;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class HotelServiceImplTest {

    @Mock
    public HotelRepository hotelRepository;

    @InjectMocks
    public HotelServiceImpl hotelService;

    @Test
    public void addHotel_Test() {
        //prepare
        Hotel hotel = getHotel();
        when(hotelRepository.save(hotel)).thenReturn(returnHotel());
        //exercise
        Hotel newHotel = hotelService.addHotel(hotel);
        //assert
        assertThat(newHotel.getId()).isEqualTo(1L);
        assertThat(newHotel.getCreatedAt()).isNotNull();
    }

    @Test
    public void getHotel_Test(){
        //prepare
        Hotel hotel = returnHotel();
        when(hotelRepository.findById(1L)).thenReturn(Optional.of(hotel));
        //exercise
        Hotel hotelDetails = hotelService.getHotelDetails(1L);
        //assert
        assertThat(hotelDetails).isEqualTo(hotel);
    }

    @Test
    public void editHotel_Test(){
        //prepare
        Hotel hotel = returnHotel();
        Hotel newHotel = getHotel();
        newHotel.setName("NEW name");
        when(hotelRepository.findById(1L)).thenReturn(Optional.of(hotel));
        //exercise
        Hotel hotelDetails = hotelService.editHotel(1L, newHotel);
        //assert
        assertThat(hotelDetails.getName()).isEqualTo("NEW name");
    }


    private Hotel getHotel() {
        Hotel hotel = Hotel.builder()
                .name("Test hotel")
                .address(Address.builder()
                        .address("address")
                        .city("city")
                        .postNumber("18000")
                        .country("serbia").build()
                ).build();
        return hotel;
    }

    private Hotel returnHotel(){
        Hotel hotel = (Hotel) Hotel.builder()
                .name("Test hotel")
                .address(Address.builder()
                        .address("address")
                        .city("city")
                        .postNumber("18000")
                        .country("serbia").build()
                )
                .id(1L)
                .createdAt(new Date())
                .build();
        return hotel;
    }
}