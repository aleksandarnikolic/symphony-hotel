package is.symphony.hotel.repository;

import is.symphony.hotel.domain.user.MetadataType;
import is.symphony.hotel.domain.user.User;
import is.symphony.hotel.domain.user.UserHotelReview;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserHotelReviewRepository extends JpaRepository<UserHotelReview, Long> {

    Optional<UserHotelReview> findById(Long id);

    List<UserHotelReview> findByHotelId(Long hotelId);

    @Query("select m.user from UserHotelMetadata m where m.userHotelReview.id = :reviewId and m.type=:metadataType")
    List<User> findUsersByReviewIdAndMetadataType(@Param("reviewId") Long reviewId, @Param("metadataType")MetadataType metadataType);

}