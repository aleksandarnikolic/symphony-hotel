package is.symphony.hotel.repository;

import is.symphony.hotel.domain.hotel.Hotel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface HotelRepository extends JpaRepository<Hotel, Long>, QuerydslPredicateExecutor<Hotel> {

    Optional<Hotel> findById(Long id);

    List<Hotel> findAllByOrderByNameAsc();
}