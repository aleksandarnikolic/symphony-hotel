package is.symphony.hotel.repository;

import is.symphony.hotel.domain.hotel.Hotel;
import is.symphony.hotel.domain.user.MetadataType;
import is.symphony.hotel.domain.user.UserHotelMetadata;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserHotelMetadataRepository extends JpaRepository<UserHotelMetadata, Long> {

    Optional<UserHotelMetadata> findById(Long id);

    UserHotelMetadata findByUserIdAndUserHotelReviewIdAndType(Long userId, Long userHotelReviewId, MetadataType type);

    List<Hotel> findByUserIdAndType(Long userId, MetadataType type);
}
