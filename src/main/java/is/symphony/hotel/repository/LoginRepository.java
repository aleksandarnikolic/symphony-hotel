package is.symphony.hotel.repository;

import is.symphony.hotel.domain.user.Login;
import is.symphony.hotel.domain.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LoginRepository extends JpaRepository<Login, Long> {

    Optional<Login> findById(Long id);


}
