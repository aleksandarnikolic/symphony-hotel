package is.symphony.hotel.service;

import is.symphony.hotel.domain.user.User;

public interface AccountService {

    User createUser(String email, String displayName, String password);

    //TODO
    User verifyEmail();

}
