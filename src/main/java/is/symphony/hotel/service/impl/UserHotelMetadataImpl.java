package is.symphony.hotel.service.impl;

import com.google.common.base.Preconditions;
import is.symphony.hotel.domain.hotel.Hotel;
import is.symphony.hotel.domain.user.MetadataType;
import is.symphony.hotel.domain.user.User;
import is.symphony.hotel.domain.user.UserHotelMetadata;
import is.symphony.hotel.domain.user.UserHotelReview;
import is.symphony.hotel.exception.BadRequestException;
import is.symphony.hotel.repository.HotelRepository;
import is.symphony.hotel.repository.UserHotelMetadataRepository;
import is.symphony.hotel.repository.UserHotelReviewRepository;
import is.symphony.hotel.repository.UserRepository;
import is.symphony.hotel.service.UserHotelMetadataService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserHotelMetadataImpl implements UserHotelMetadataService {

    private UserHotelReviewRepository userHotelReviewRepository;
    private UserHotelMetadataRepository userHotelMetadataRepository;
    private UserRepository userRepository;
    private HotelRepository hotelRepository;

    public UserHotelMetadataImpl(UserHotelReviewRepository userHotelReviewRepository, UserHotelMetadataRepository userHotelMetadataRepository, UserRepository userRepository, HotelRepository hotelRepository) {
        this.userHotelReviewRepository = userHotelReviewRepository;
        this.userHotelMetadataRepository = userHotelMetadataRepository;
        this.userRepository = userRepository;
        this.hotelRepository = hotelRepository;
    }

    @Override
    public List<Hotel> getFavoriteHotels(Long userId) {
        Preconditions.checkNotNull(userId);
        return userHotelMetadataRepository.findByUserIdAndType(userId, MetadataType.FAVORITE);
    }

    @Override
    public UserHotelMetadata addToFavorite(Long userId, Long hotelId) {
        Preconditions.checkNotNull(userId, hotelId);
        return addMetadata(userId, hotelId, null, MetadataType.LIKE);
    }

    @Override
    public void removeFromFavorite(Long userId, Long hotelId) {
        Preconditions.checkNotNull(userId, hotelId);
        deleteMetadata(userId, hotelId, null, MetadataType.LIKE);
    }

    @Override
    public UserHotelMetadata likeReview(Long userId, Long userHotelReviewId) {
        Preconditions.checkNotNull(userId);
        Preconditions.checkNotNull(userHotelReviewId);
        return addMetadata(userId, null, userHotelReviewId, MetadataType.LIKE);
    }

    @Override
    public void removeLike(Long userId, Long userHotelReviewId) {
        Preconditions.checkNotNull(userId);
        Preconditions.checkNotNull(userHotelReviewId);
        deleteMetadata(userId, null, userHotelReviewId, MetadataType.LIKE);
    }

    @Override
    public UserHotelMetadata dislikeReview(Long userId, Long userHotelReviewId) {
        Preconditions.checkNotNull(userId);
        Preconditions.checkNotNull(userHotelReviewId);
        return addMetadata(userId, null, userHotelReviewId, MetadataType.DISLIKE);
    }

    @Override
    public void removeDislike(Long userId, Long userHotelReviewId) {
        Preconditions.checkNotNull(userId);
        Preconditions.checkNotNull(userHotelReviewId);
        deleteMetadata(userId, null, userHotelReviewId, MetadataType.DISLIKE);
    }

    /**
     * @param userId
     * @param hotelId           should be null for review metadata and not null if it's favorite hotel
     * @param userHotelReviewId
     * @param metadataType
     * @return
     */
    private UserHotelMetadata addMetadata(Long userId, Long hotelId, Long userHotelReviewId, MetadataType metadataType) {
        User user = userRepository.findById(userId).orElseThrow(() -> new BadRequestException("User not found"));
        UserHotelReview review = null;
        Hotel hotel = null;
        if (userHotelReviewId != null) {
            review = userHotelReviewRepository.findById(userHotelReviewId).orElseThrow(() -> new BadRequestException("Review not found"));
        }
        if (hotelId != null) {
            hotel = hotelRepository.findById(hotelId).orElseThrow(() -> new BadRequestException("Hotel not found"));
        }

        UserHotelMetadata metadata = UserHotelMetadata.builder()
                .userHotelReview(review)
                .hotel(hotel)
                .user(user)
                .type(metadataType)
                .build();

        UserHotelMetadata newMetadata = userHotelMetadataRepository.save(metadata);
        return newMetadata;
    }

    /**
     * @param userId
     * @param hotelId           should be null for review metadata and not null if it's favorite hotel
     * @param userHotelReviewId
     * @param metadataType
     */
    private void deleteMetadata(Long userId, Long hotelId, Long userHotelReviewId, MetadataType metadataType) {
        User user = userRepository.findById(userId).orElseThrow(() -> new BadRequestException("User not found"));
        UserHotelReview review = null;
        Hotel hotel = null;
        if (userHotelReviewId != null) {
            review = userHotelReviewRepository.findById(userHotelReviewId).orElseThrow(() -> new BadRequestException("Review not found"));
        }
        if (hotelId != null) {
            hotel = hotelRepository.findById(hotelId).orElseThrow(() -> new BadRequestException("Hotel not found"));
        }
        UserHotelMetadata metadata = userHotelMetadataRepository.findByUserIdAndUserHotelReviewIdAndType(userId, review.getId(), metadataType);
        userHotelMetadataRepository.delete(metadata);
    }
}
