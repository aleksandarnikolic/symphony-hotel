package is.symphony.hotel.service.impl;

import com.google.common.base.Preconditions;
import is.symphony.hotel.domain.hotel.Hotel;
import is.symphony.hotel.domain.user.MetadataType;
import is.symphony.hotel.domain.user.User;
import is.symphony.hotel.domain.user.UserHotelReview;
import is.symphony.hotel.exception.BadRequestException;
import is.symphony.hotel.repository.HotelRepository;
import is.symphony.hotel.repository.UserHotelReviewRepository;
import is.symphony.hotel.repository.UserRepository;
import is.symphony.hotel.service.UserHotelReviewService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserHotelReviewImpl implements UserHotelReviewService {

    private UserHotelReviewRepository userHotelReviewRepository;
    private UserRepository userRepository;
    private HotelRepository hotelRepository;

    public UserHotelReviewImpl(UserHotelReviewRepository userHotelReviewRepository, UserRepository userRepository, HotelRepository hotelRepository){
        this.userHotelReviewRepository = userHotelReviewRepository;
        this.userRepository = userRepository;
        this.hotelRepository = hotelRepository;
    }

    @Override
    public UserHotelReview createUserHotelReview(Long userId, Long hotelId, UserHotelReview review) {
        Preconditions.checkNotNull(userId);
        Preconditions.checkNotNull(hotelId);
        Preconditions.checkNotNull(review);
        User user = userRepository.findById(userId).orElseThrow(() -> new BadRequestException("User not found"));
        review.setUser(user);
        Hotel hotel = hotelRepository.findById(hotelId).orElseThrow(()-> new BadRequestException("Hotel not found"));
        review.setHotel(hotel);
        return userHotelReviewRepository.save(review);
    }

    @Override
    public List<UserHotelReview> getAllHotelReviews(Long hotelId) {
        Preconditions.checkNotNull(hotelId);
        return userHotelReviewRepository.findByHotelId(hotelId);
    }

    @Override
    public List<User> getUsersWhoLikedHotelReview(Long userHotelReviewId) {
        Preconditions.checkNotNull(userHotelReviewId);
        return userHotelReviewRepository.findUsersByReviewIdAndMetadataType(userHotelReviewId, MetadataType.LIKE);
    }

    @Override
    public List<User> getUsersWhoDislikedHotelReview(Long userHotelReviewId) {
        Preconditions.checkNotNull(userHotelReviewId);
        return userHotelReviewRepository.findUsersByReviewIdAndMetadataType(userHotelReviewId, MetadataType.DISLIKE);
    }
}
