package is.symphony.hotel.service.impl;

import com.google.common.base.Preconditions;
import is.symphony.hotel.domain.user.Login;
import is.symphony.hotel.domain.user.User;
import is.symphony.hotel.repository.LoginRepository;
import is.symphony.hotel.repository.UserRepository;
import is.symphony.hotel.service.AccountService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    public LoginRepository loginRepository;
    public UserRepository userRepository;

    public AccountServiceImpl(LoginRepository loginRepository, UserRepository userRepository){
        this.loginRepository = loginRepository;
        this.userRepository = userRepository;
    }

    @Override
    public User createUser(String email, String displayName, String password) {
        Preconditions.checkNotNull(email);
        Preconditions.checkNotNull(displayName);
        Preconditions.checkNotNull(password);
        Login login = Login.builder().email(email).password(password).build();
        User user = User.builder().displayName(displayName).build();

        User newUser = userRepository.save(user);
        login.setUser(user);

        Login newLogin = loginRepository.save(login);
        return newUser;
    }

    @Override
    public User verifyEmail() {
        return null;
    }
}
