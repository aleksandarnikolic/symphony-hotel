package is.symphony.hotel.service.impl;

import com.google.common.base.Preconditions;
import is.symphony.hotel.domain.user.User;
import is.symphony.hotel.repository.UserRepository;
import is.symphony.hotel.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User getUserById(Long id) {
        Preconditions.checkNotNull(id);
        return userRepository.findById(id).get();
    }
}
