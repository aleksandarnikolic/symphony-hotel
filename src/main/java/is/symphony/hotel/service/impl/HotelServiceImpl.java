package is.symphony.hotel.service.impl;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import is.symphony.hotel.domain.hotel.Hotel;
import is.symphony.hotel.domain.hotel.QHotel;
import is.symphony.hotel.dto.SearchHotelRequest;
import is.symphony.hotel.exception.BadRequestException;
import is.symphony.hotel.repository.HotelRepository;
import is.symphony.hotel.service.HotelService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.awt.print.Pageable;
import java.util.List;

@Service
@Transactional
public class HotelServiceImpl implements HotelService {

    private HotelRepository hotelRepository;

    public HotelServiceImpl(HotelRepository hotelRepository) {
        this.hotelRepository = hotelRepository;
    }

    @Override
    public Hotel addHotel(Hotel hotel) {
        Preconditions.checkNotNull(hotel);
        return hotelRepository.save(hotel);
    }

    @Override
    public Hotel getHotelDetails(Long id) {
        Preconditions.checkNotNull(id);
        return hotelRepository.findById(id).orElseThrow(() -> new BadRequestException("Hotel does not exist"));
    }

    @Override
    public List<Hotel> getAll(Pageable pageable) {
        return hotelRepository.findAllByOrderByNameAsc();
    }

    @Override
    public Hotel editHotel(Long id, Hotel hotel) {
        Preconditions.checkNotNull(id);
        Preconditions.checkNotNull(hotel);
        Hotel oldHotel = hotelRepository.findById(id).orElseThrow(() -> new BadRequestException("Hotel does not exist"));
        oldHotel.setName(hotel.getName());
        oldHotel.setAddress(hotel.getAddress());
        return oldHotel;
    }

    @Override
    public List<Hotel> search(SearchHotelRequest request) {
        Preconditions.checkNotNull(request);
        Iterable<Hotel> all = hotelRepository.findAll(findByNameOrCity(request.getName(), request.getCity()));
        return Lists.newArrayList(all);
    }

    public static Predicate findByNameOrCity(String name, String city) {
        if(name != null && !name.isEmpty()) {
            name = name.trim();
            QHotel project = QHotel.hotel;
            BooleanBuilder searchCriteria = new BooleanBuilder();
            searchCriteria.and(project.name.containsIgnoreCase(name)).or(project.address.city.containsIgnoreCase(city));
            return searchCriteria;
        }
        return null;
    }
}
