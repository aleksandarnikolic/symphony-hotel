package is.symphony.hotel.service;

import is.symphony.hotel.domain.user.User;

public interface UserService {

    User getUserById(Long id);

}
