package is.symphony.hotel.service;

import is.symphony.hotel.domain.user.User;
import is.symphony.hotel.domain.user.UserHotelReview;
import java.util.List;

public interface UserHotelReviewService {

    UserHotelReview createUserHotelReview(Long userId, Long hotelId, UserHotelReview review);
    List<UserHotelReview> getAllHotelReviews(Long hotelId);
    List<User> getUsersWhoLikedHotelReview(Long userHotelReviewId);
    List<User> getUsersWhoDislikedHotelReview(Long userHotelReviewId);

}
