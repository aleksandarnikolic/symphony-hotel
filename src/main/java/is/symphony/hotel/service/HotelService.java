package is.symphony.hotel.service;

import is.symphony.hotel.domain.hotel.Hotel;
import is.symphony.hotel.dto.SearchHotelRequest;

import java.awt.print.Pageable;
import java.util.List;

public interface HotelService {

    Hotel addHotel(Hotel hotel);
    Hotel getHotelDetails(Long id);
    List<Hotel> getAll(Pageable pageable);
    Hotel editHotel(Long id, Hotel hotel);
    List<Hotel> search(SearchHotelRequest request);

}
