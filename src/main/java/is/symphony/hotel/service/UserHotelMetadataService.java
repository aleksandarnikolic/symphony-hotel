package is.symphony.hotel.service;

import is.symphony.hotel.domain.hotel.Hotel;
import is.symphony.hotel.domain.user.UserHotelMetadata;

import java.util.List;

public interface UserHotelMetadataService {

    //Favorite func
    List<Hotel> getFavoriteHotels(Long userId);
    UserHotelMetadata addToFavorite(Long userId, Long hotelId);
    void removeFromFavorite(Long userId, Long hotelId);

    //Like/dislike review func
    UserHotelMetadata likeReview(Long userId, Long userHotelReviewId);
    void removeLike(Long userId, Long userHotelReviewId);
    UserHotelMetadata dislikeReview(Long userId, Long userHotelReviewId);
    void removeDislike(Long userId, Long userHotelReviewId);

}
