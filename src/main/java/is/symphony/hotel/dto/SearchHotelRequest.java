package is.symphony.hotel.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SearchHotelRequest {

    private String name;
    private String city;
}
