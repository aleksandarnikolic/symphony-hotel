package is.symphony.hotel.aop;

import is.symphony.hotel.exception.BadRequestWithArgsException;
import is.symphony.hotel.exception.BadRequestWithMessageException;
import is.symphony.hotel.exception.BeanFieldError;
import is.symphony.hotel.exception.DeleteEntityException;
import is.symphony.hotel.exception.ErrorMessage;
import is.symphony.hotel.exception.GeneralError;
import is.symphony.hotel.exception.PersistEntityException;
import is.symphony.hotel.exception.ResourceNotFoundException;
import is.symphony.hotel.exception.UnprocessableRequestException;
import is.symphony.hotel.exception.UpdateEntityException;
import is.symphony.hotel.exception.ValidationException;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Component
@ControllerAdvice
public class ExceptionHandlerControllerAdvice extends ResponseEntityExceptionHandler {

    @Autowired
    private MessageSource messageSource;

    /**
     * 400. Is triggered if no other handler is triggered
     */
    @ExceptionHandler
    public ResponseEntity<Object> badRequestHandler(final Exception exception) {
        final HttpStatus status = HttpStatus.BAD_REQUEST;
        final ErrorMessage errorMessage;
        if (exception instanceof BadRequestWithMessageException) {
            final BadRequestWithMessageException badRequestWithMessageException = (BadRequestWithMessageException) exception;
            errorMessage = createGeneralErrorMessages(badRequestWithMessageException.getMessage(),
                    badRequestWithMessageException.getMessageText());
        } else if(exception instanceof BadRequestWithArgsException) {
            final BadRequestWithArgsException badRequestWithMessageException = (BadRequestWithArgsException) exception;
            errorMessage = createGeneralErrorMessagesWithArgs(badRequestWithMessageException.getMessage(), badRequestWithMessageException.getArguments());
        } else {
            errorMessage = createGeneralErrorMessages(exception.getMessage());
        }
        return new ResponseEntity<>(errorMessage, status);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            final MethodArgumentNotValidException ex,
            final HttpHeaders headers,
            final HttpStatus status,
            final WebRequest request) {
        return createValidationErrorMessage(ex.getBindingResult());
    }

    /**
     * 404
     */
    @ExceptionHandler({ ResourceNotFoundException.class })
    public ResponseEntity<Object> resourceNotFoundHandler(final Exception exception) {
        final HttpStatus status = HttpStatus.NOT_FOUND;
        final ErrorMessage errorMessage = createGeneralErrorMessages(exception.getMessage());
        return new ResponseEntity<>(errorMessage, status);
    }

    /**
     * 409
     */
    @ExceptionHandler({ PersistEntityException.class, UpdateEntityException.class, DeleteEntityException.class,
            ConstraintViolationException.class })
    public ResponseEntity<Object> databaseOperationHandler(final Exception exception) {
        final HttpStatus status = HttpStatus.CONFLICT;
        final ErrorMessage errorMessage = createGeneralErrorMessages(exception.getMessage());
        return new ResponseEntity<>(errorMessage, status);
    }

    /**
     * 422 for unprocessable requests
     */
    @ExceptionHandler({ UnprocessableRequestException.class })
    public ResponseEntity<Object> unprocessableRequestHandler(final Exception exception) {
        final HttpStatus status = HttpStatus.UNPROCESSABLE_ENTITY;
        final ErrorMessage errorMessage = createGeneralErrorMessages(exception.getMessage());
        return new ResponseEntity<>(errorMessage, status);
    }

    /**
     * 422 for bean validation
     */
    @ExceptionHandler({ ValidationException.class })
    public ResponseEntity<Object> customValidationFailedHandler(final ValidationException exception) {
        return createValidationErrorMessage(exception.getBindingResult());
    }

    /**
     * Creates validationErrorMessage from the BindingResult
     */
    private ResponseEntity<Object> createValidationErrorMessage(final BindingResult result) {
        final HttpStatus status = HttpStatus.UNPROCESSABLE_ENTITY;
        final ErrorMessage errorMessage = new ErrorMessage();
        final List<BeanFieldError> allErrorMessages = new ArrayList<>();
        final Locale locale = LocaleContextHolder.getLocale();
        result.getGlobalErrors().forEach(error -> {
            final BeanFieldError globalErrorMessage = new BeanFieldError();
            globalErrorMessage.setErrorType(StringUtils.isEmpty(error.getDefaultMessage()) ? error.getCode()
                    : error.getDefaultMessage());
            final String translatedMessage = messageSource.getMessage(globalErrorMessage.getErrorType(), null, globalErrorMessage.getErrorType(), locale);
            globalErrorMessage.setMessageText(translatedMessage);
            allErrorMessages.add(globalErrorMessage);
        });
        result.getFieldErrors().forEach(error -> {
            final BeanFieldError filedErrorMessage = new BeanFieldError();
            filedErrorMessage.setFieldName(error.getField());
            filedErrorMessage.setErrorType(StringUtils.isEmpty(error.getDefaultMessage()) ? error.getCode()
                    : error.getDefaultMessage());
            final String translatedMessage = messageSource.getMessage(filedErrorMessage.getErrorType(), null, filedErrorMessage.getErrorType(), locale);
            filedErrorMessage.setMessageText(translatedMessage);
            allErrorMessages.add(filedErrorMessage);
        });
        errorMessage.setFieldErrors(allErrorMessages);
        return new ResponseEntity<>(errorMessage, status);
    }

    /**
     * Gets the locale for the user and creates localized message
     * @param exceptionMessageKey
     * @return
     */
    private ErrorMessage createGeneralErrorMessages(final String exceptionMessageKey) {
        return createGeneralErrorMessagesWithArgs(exceptionMessageKey, null);
    }

    private ErrorMessage createGeneralErrorMessagesWithArgs(final String exceptionMessageKey, final String[] arguments){
        final ErrorMessage errorMessage = new ErrorMessage();
        final Locale locale = LocaleContextHolder.getLocale();
        final String translatedMessage = messageSource.getMessage(exceptionMessageKey, arguments, exceptionMessageKey, locale);
        final GeneralError generalErrorMessage = new GeneralError(exceptionMessageKey, translatedMessage);
        errorMessage.setGeneralError(generalErrorMessage);
        return errorMessage;
    }

    private ErrorMessage createGeneralErrorMessages(final String exceptionMessageKey, final String exceptionMessageText) {
        final GeneralError generalErrorMessage = new GeneralError(exceptionMessageKey, exceptionMessageText);
        final ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setGeneralError(generalErrorMessage);
        return errorMessage;
    }

}
