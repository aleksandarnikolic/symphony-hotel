package is.symphony.hotel.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
@Getter
public class BadRequestWithArgsException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private static final String DEFAULT_MESSAGE = "badRequest";

    private String message;
    private String[] arguments;

    public BadRequestWithArgsException() {
        super(DEFAULT_MESSAGE);
    }

    public BadRequestWithArgsException(final String message, final String[] arguments) {
        this.message = message;
        this.arguments = arguments;
    }
}
