package is.symphony.hotel.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception when trying to persist the entity to the DB
 */
@ResponseStatus(HttpStatus.CONFLICT)
public class PersistEntityException extends RuntimeException {

    private static final long serialVersionUID = 3113582217910858791L;
    private static final String DEFAULT_MESSAGE = "entityCannotBePersisted";

    public PersistEntityException() {
        super(DEFAULT_MESSAGE);
    }

    public PersistEntityException(final String message) {
        super(message);
    }

}
