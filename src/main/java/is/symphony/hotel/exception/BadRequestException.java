package is.symphony.hotel.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Request sent with bad parameters
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {

    private static final long serialVersionUID = -8438278447163733946L;
    private static final String DEFAULT_MESSAGE = "badRequest";

    public BadRequestException() {
        super(DEFAULT_MESSAGE);
    }

    public BadRequestException(final String message) {
        super(message);
    }

}
