package is.symphony.hotel.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception while trying to update entity in the DB
 */
@ResponseStatus(HttpStatus.CONFLICT)
public class UpdateEntityException extends RuntimeException {

    private static final long serialVersionUID = -7390794065561455673L;
    private static final String DEFAULT_MESSAGE = "entityCannotBeUpdated";

    public UpdateEntityException() {
        super(DEFAULT_MESSAGE);
    }

    public UpdateEntityException(final String message) {
        super(message);
    }

}
