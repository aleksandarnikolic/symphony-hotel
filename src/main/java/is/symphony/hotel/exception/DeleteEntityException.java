package is.symphony.hotel.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception when trying to delete entity from the DB
 */
@ResponseStatus(HttpStatus.CONFLICT)
public class DeleteEntityException extends RuntimeException {

    private static final long serialVersionUID = 7075499780179060860L;
    private static final String DEFAULT_MESSAGE = "entityCannotBeDeleted";

    public DeleteEntityException() {
        super(DEFAULT_MESSAGE);
    }

    public DeleteEntityException(final String message) {
        super(message);
    }

}
