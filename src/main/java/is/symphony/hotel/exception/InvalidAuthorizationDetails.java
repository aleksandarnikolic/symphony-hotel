package is.symphony.hotel.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.UNAUTHORIZED)
public class InvalidAuthorizationDetails extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public static final String ERROR_MESSAGE = "invalidAuthorizationDetails";

    public InvalidAuthorizationDetails() {
        super(ERROR_MESSAGE);
    }

    public InvalidAuthorizationDetails(final String message) {
        super(message);
    }
}
