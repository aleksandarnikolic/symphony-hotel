package is.symphony.hotel.exception;

/**
 * Requested service is not available
 */
public class ServiceNotAvailableException extends RuntimeException {

    private static final long serialVersionUID = 7295457095342485910L;
    private static final String DEFAULT_MESSAGE = "serviceNotAvailable";

    public ServiceNotAvailableException() {
        super(DEFAULT_MESSAGE);
    }

    public ServiceNotAvailableException(final String message) {
        super(message);
    }

}
