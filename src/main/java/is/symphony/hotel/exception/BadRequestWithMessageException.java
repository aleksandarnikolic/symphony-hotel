package is.symphony.hotel.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Request sent with bad parameters
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
@Getter
public class BadRequestWithMessageException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private static final String DEFAULT_MESSAGE = "badRequest";

    private String message;
    private String messageText;

    public BadRequestWithMessageException() {
        super(DEFAULT_MESSAGE);
    }

    public BadRequestWithMessageException(final String message) {
        this.message = message;
    }

    public BadRequestWithMessageException(final String message, final String messageText) {
        this.message = message;
        this.messageText = messageText;
    }

}
