package is.symphony.hotel.exception;

import lombok.Getter;
import org.springframework.validation.BindingResult;

/**
 * Exception when the validation of request fails (with fieldErrors for e.g.). This is further processed by ControllerAdvice
 * so the formatted JSON response returns to the client
 */

@Getter
public class ValidationException extends RuntimeException {

    private static final long serialVersionUID = -6508077551232552233L;
    private final BindingResult bindingResult;

    public ValidationException(final BindingResult bindingResult) {
        this.bindingResult = bindingResult;
    }
}
