package is.symphony.hotel.resource;

import is.symphony.hotel.domain.user.User;
import is.symphony.hotel.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserResource {

    private UserService userService;

    public UserResource (UserService userService){
        this.userService = userService;
    }

    @RequestMapping(value = Path.USER_GET, method = RequestMethod.GET)
    public ResponseEntity<User> userGET(@PathVariable Long userId){
        return ResponseEntity.ok(userService.getUserById(userId));
    }

}
