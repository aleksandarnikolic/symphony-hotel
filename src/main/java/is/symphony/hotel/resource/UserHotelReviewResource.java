package is.symphony.hotel.resource;

import is.symphony.hotel.domain.user.User;
import is.symphony.hotel.domain.user.UserHotelReview;
import is.symphony.hotel.service.UserHotelReviewService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class UserHotelReviewResource {

    //TODO get from spring security
    Long userId = 0L;

    private UserHotelReviewService userHotelReviewService;

    public UserHotelReviewResource(UserHotelReviewService userHotelReviewService){
        this.userHotelReviewService = userHotelReviewService;
    }

    @RequestMapping(value = Path.USER_HOTEL_REVIEW_POST, method = RequestMethod.POST)
    public ResponseEntity<UserHotelReview> userPOST(@PathVariable Long hotelId, @Valid @RequestBody UserHotelReview request){
        return ResponseEntity.ok(userHotelReviewService.createUserHotelReview(userId, hotelId, request));
    }

    @RequestMapping(value = Path.USERS_WHO_LIKED_HOTEL_REVIEW, method = RequestMethod.GET)
    public ResponseEntity<List<User>> usersWhoLikedReviewPOST(@PathVariable Long reviewId){
        return ResponseEntity.ok(userHotelReviewService.getUsersWhoLikedHotelReview(reviewId));
    }

    @RequestMapping(value = Path.USERS_WHO_DISLIKED_HOTEL_REVIEW, method = RequestMethod.GET)
    public ResponseEntity<List<User>> usersWhoDislikedReviewPOST(@PathVariable Long reviewId){
        return ResponseEntity.ok(userHotelReviewService.getUsersWhoDislikedHotelReview(reviewId));
    }

    @RequestMapping(value = Path.USER_HOTEL_REVIEWS_GET, method = RequestMethod.GET)
    public ResponseEntity<List<UserHotelReview>> allHotelReviewsGET(@PathVariable Long hotelId){
        return ResponseEntity.ok(userHotelReviewService.getAllHotelReviews(hotelId));
    }

}
