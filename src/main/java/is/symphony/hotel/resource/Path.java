package is.symphony.hotel.resource;

public class Path {

    public static final String USER_GET = "/user/{userId}";

    public static final String HOTEL_POST = "/hotel";
    public static final String HOTEL_GET = "/hotel/{hotelId}";
    public static final String HOTEL_EDIT = "/hotel/{hotelId}";
    public static final String HOTEL_GET_ALL = "/hotel/all";
    public static final String HOTEL_SEARCH = "/hotel/search";

    public static final String METADATA_LIKE_REVIEW_GET = "/metadata/review/{reviewId}/like";
    public static final String METADATA_REMOVE_LIKE_FOR_REVIEW_GET = "/metadata/review/{reviewId}/like/remove";
    public static final String METADATA_DISLIKE_REVIEW_GET = "/metadata/review/{reviewId}/dislike";
    public static final String METADATA_REMOVE_DISLIKE_FOR_REVIEW_GET = "/metadata/review/{reviewId}/dislike/remove";
    public static final String METADATA_FAVORITE_HOTELS_GET = "/metadata/favorite/hotel/all";
    public static final String METADATA_ADD_FAVORITE_HOTEL_GET = "/metadata/favorite/hotel/{hotelId}/add";
    public static final String METADATA_REMOVE_HOTEL_FROM_FAVORITE_GET = "/metadata/favorite/hotel/{hotelId}/remove";

    public static final String USER_HOTEL_REVIEW_POST = "/user/hotel/{hotelId}/review";
    public static final String USER_HOTEL_REVIEWS_GET = "/hotel/{hotelId}/review/all";
    public static final String USERS_WHO_LIKED_HOTEL_REVIEW = "/user/review/{reviewId}/like/all";
    public static final String USERS_WHO_DISLIKED_HOTEL_REVIEW = "/user/review/{reviewId}/dislike/all";
}
