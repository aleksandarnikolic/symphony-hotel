package is.symphony.hotel.resource;

import is.symphony.hotel.domain.hotel.Hotel;
import is.symphony.hotel.dto.SearchHotelRequest;
import is.symphony.hotel.service.HotelService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class HotelResource {

    private HotelService hotelService;

    public HotelResource(HotelService hotelService){
        this.hotelService = hotelService;
    }

    @RequestMapping(value = Path.HOTEL_POST, method = RequestMethod.GET)
    public ResponseEntity<Hotel> userPOST( @Valid @RequestBody Hotel request){
        return ResponseEntity.ok(hotelService.addHotel(request));
    }

    @RequestMapping(value = Path.HOTEL_GET, method = RequestMethod.GET)
    public ResponseEntity<Hotel> userGET(@PathVariable Long hotelId){
        return ResponseEntity.ok(hotelService.getHotelDetails(hotelId));
    }

    @RequestMapping(value = Path.HOTEL_GET_ALL, method = RequestMethod.GET)
    public ResponseEntity<List<Hotel>> userGET(){
        //TODO pass pageable object
        return ResponseEntity.ok(hotelService.getAll(null));
    }

    @RequestMapping(value = Path.HOTEL_EDIT, method = RequestMethod.PUT)
    public ResponseEntity<Hotel> editPUT(@PathVariable Long hotelId, @Valid @RequestBody Hotel request){
        return ResponseEntity.ok(hotelService.editHotel(hotelId, request));
    }

    @RequestMapping(value = Path.HOTEL_SEARCH, method = RequestMethod.POST)
    public ResponseEntity<List<Hotel>> searchByNameOrCity(@Valid @RequestBody SearchHotelRequest request){
        return ResponseEntity.ok(hotelService.search(request));
    }

}
