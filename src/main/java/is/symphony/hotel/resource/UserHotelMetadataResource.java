package is.symphony.hotel.resource;

import is.symphony.hotel.domain.hotel.Hotel;
import is.symphony.hotel.domain.user.UserHotelMetadata;
import is.symphony.hotel.service.UserHotelMetadataService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserHotelMetadataResource {

    //TODO remove when spring security
    private Long userId = 0L;

    private UserHotelMetadataService userHotelMetadataService;

    public UserHotelMetadataResource(UserHotelMetadataService userHotelMetadataService){
        this.userHotelMetadataService = userHotelMetadataService;
    }

    @RequestMapping(value = Path.METADATA_FAVORITE_HOTELS_GET, method = RequestMethod.GET)
    public ResponseEntity<List<Hotel>> favoriteHotelsGET(){
        return ResponseEntity.ok(userHotelMetadataService.getFavoriteHotels(userId));
    }

    @RequestMapping(value = Path.METADATA_ADD_FAVORITE_HOTEL_GET, method = RequestMethod.GET)
    public ResponseEntity<UserHotelMetadata> favoriteHotelGET(@PathVariable Long hotelId){
        return ResponseEntity.ok(userHotelMetadataService.addToFavorite(userId, hotelId));
    }

    @RequestMapping(value = Path.METADATA_REMOVE_HOTEL_FROM_FAVORITE_GET, method = RequestMethod.DELETE)
    public ResponseEntity removeHotelFromFavoriteGET(@PathVariable Long hotelId){
        userHotelMetadataService.removeFromFavorite(userId, hotelId);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = Path.METADATA_LIKE_REVIEW_GET, method = RequestMethod.GET)
    public ResponseEntity<UserHotelMetadata> likeReviewGET(@PathVariable Long reviewId){
        return ResponseEntity.ok(userHotelMetadataService.likeReview(userId, reviewId));
    }

    @RequestMapping(value = Path.METADATA_REMOVE_LIKE_FOR_REVIEW_GET, method = RequestMethod.DELETE)
    public ResponseEntity removeLikeFromReviewGET(@PathVariable Long reviewId){
        userHotelMetadataService.removeLike(userId, reviewId);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = Path.METADATA_DISLIKE_REVIEW_GET, method = RequestMethod.GET)
    public ResponseEntity<UserHotelMetadata> dislikeReviewGET(@PathVariable Long reviewId){
        return ResponseEntity.ok(userHotelMetadataService.dislikeReview(userId, reviewId));
    }

    @RequestMapping(value = Path.METADATA_REMOVE_DISLIKE_FOR_REVIEW_GET, method = RequestMethod.DELETE)
    public ResponseEntity removeDislikeFromReviewGET(@PathVariable Long reviewId){
        userHotelMetadataService.removeDislike(userId, reviewId);
        return ResponseEntity.ok().build();
    }
}
