package is.symphony.hotel.domain.hotel;

import is.symphony.hotel.domain.Address;
import is.symphony.hotel.domain.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Embedded;
import javax.persistence.Entity;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@Entity
public class Hotel extends BaseEntity {

    private String name;

    @Embedded
    private Address address;
}
