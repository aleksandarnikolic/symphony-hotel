package is.symphony.hotel.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;


@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class Address {

    private String country;
    private String city;
    private String postNumber;
    private String address;

    /**
     * Concatenate address fields in format: "Friedrichstr. 123, 10117 Berlin, Germany"
     * @return full address
     */
    public String getFullAddress() {
        StringBuilder fullAddress = new StringBuilder();
        fullAddress.append( this.address).append(", ").append(this.postNumber).append(" ").append(this.city).append(", ").append(this.country);
        return fullAddress.toString();
    }
}