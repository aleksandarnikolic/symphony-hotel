package is.symphony.hotel.domain.user;

import is.symphony.hotel.domain.Address;
import is.symphony.hotel.domain.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Embedded;
import javax.persistence.Entity;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@Entity
public class User extends BaseEntity {

    private String fullName;
    private String displayName;

    @Embedded
    private Address address;

    private String mobile;

    private String email;

}
