package is.symphony.hotel.domain.user;

import is.symphony.hotel.domain.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@Entity
public class Login extends BaseEntity {

    @ManyToOne(fetch = FetchType.EAGER)
    private User user;

    @Enumerated(EnumType.STRING)
    private Role role;

    @Enumerated(EnumType.STRING)
    private UserStatus userStatus;

    @Column(unique = true, nullable = false)
    private String email;

    @Column(unique = true, nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(unique = true)
    private String emailVerificationToken;

    private LocalDateTime lastLoginDate;

    private LocalDateTime registrationDate;


    public enum Role {

        SUPER_ADMIN("Admin"),

        USER("User"),

        // do not use it in database
        ANONYMOUS("Anonymous"),;

        private String workRole;

        Role(String workRole) {
            this.workRole = workRole;
        }

        public String getWorkRole() {
            return workRole;
        }

    }

}
