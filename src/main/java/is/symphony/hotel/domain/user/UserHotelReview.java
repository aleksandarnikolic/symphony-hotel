package is.symphony.hotel.domain.user;

import is.symphony.hotel.domain.BaseEntity;
import is.symphony.hotel.domain.hotel.Hotel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@Entity
public class UserHotelReview extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    private Hotel hotel;

    private String description;

    //rating 0-5
    private Long rating;

}
