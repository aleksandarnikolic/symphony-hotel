package is.symphony.hotel.domain.user;

public enum UserStatus {

    PENDING,
    ACTIVE,
    DELETED,
    DEACTIVATED,
    ;

}
